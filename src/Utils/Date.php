<?php

namespace dlouhy\UtilsBundle\Utils;

final class Date
{
    /**
     * @param        $array
     * @param string $last
     * @return string
     */
    public static function age($array, $last = ' and ')
    {
        if (count($array = array_filter(array_unique((array) $array), 'strlen')) >= 3) {
            $array = [implode(', ', array_slice($array, 0, -1)), implode('', array_slice($array, -1))];
        }

        return implode($last, $array);
    }
}
